# README #

### Truecasing ###

Truecasing is the process of restoring case information to badly-cased or non-cased text [[quoted from "tRuEcasIng"](http://dl.acm.org/citation.cfm?id=1075096.1075116)]. Although other libraries exists which does this as part of a bigger package, I could not find something for Java which just focused on true casing for English language. 

This package uses the Random Forest machine learning algorithm as implemented in [Weka machine learning toolkit](http://www.cs.waikato.ac.nz/ml/weka/) and sentence analysis using [Stanford POS tagger](http://nlp.stanford.edu/software/tagger.shtml).

* Current Version of truecaser is 0.5

### How do I get set up? ###

* The source contains the maven pom required build truecaser.jar. Please note you will need to install the jar files for Weka toolkit and Stanford POS tagger in your local maven repository. Alternatively you can download the pre-built jar from downloads area.

* Dependencies (tested with, other versions should work)
    * Weka tookit (3.7.11)
    * Stanford POS tagger (3.4.1)
    * Spring framework 3.1

* There are no tests yet


### Who do I talk to? ###

* Repo owner