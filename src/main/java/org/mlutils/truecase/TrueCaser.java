package org.mlutils.truecase;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import weka.classifiers.Classifier;
import weka.core.Instance;
import weka.core.Instances;

/**
 * Classify a list of {@link WordFeatures} using a previously trained classifier and compose 
 * a true cased sentence with appropriate formatting.
 * 
 * @author francois
 *
 */
public class TrueCaser {
	
	private static final Logger log = LoggerFactory.getLogger(TrueCaser.class);
	
	private final Classifier classifier;
	
	public TrueCaser(Classifier classifier) {
		this.classifier = classifier;
	}
	
	class WordSpacer {
		
		private boolean	prevSpaceAfter = false;
		private boolean spaceBefore = false;
		private boolean	spaceAfter = false;
		
		void update(WordFeatures wordFeature) {
			
			prevSpaceAfter = spaceAfter;
			
			switch (wordFeature.getPartOfSpeech()) {
			
				case SENTENCE_TERMINATOR:
				case PUNC_COMMA:
				case PUNC_SCOLON:
				case PUNC_COLON:
				case RRB:
				case RSB:
				case RCB:
				case POSSESSIVE_ENDING:					
				case CLOSE_QUOTE:					
					spaceBefore = false;
					spaceAfter = true;
					break;
					
				case LRB:
				case LSB:
				case LCB:
				case DOLLAR:
				case OPEN_QUOTE:
					spaceBefore = true;
					spaceAfter = false;
					break;
					
				default:
					spaceBefore = true;
					spaceAfter = "\n".equals(wordFeature.getWord()) ? false : true;
					break;
			}
		}
		
		boolean space() {
			return prevSpaceAfter && spaceBefore;
		}
	}
	
	public String doTrueCase(List<WordFeatures> wordFeatures, Instances instances) {
		
		StringBuilder stringBuilder = new StringBuilder();
		
		WordSpacer wordSpacer = new WordSpacer();
		
		for (int i = 0; i < instances.size(); i++) {
			
			Instance instance = instances.get(i);
			WordFeatures wordFeature = wordFeatures.get(i);
			
			wordSpacer.update(wordFeature);
			
			try {
				
				int classification = (int) classifier.classifyInstance(instance);
				WordCaseType wordCaseType = WordCaseType.fromInt(classification);
				
				String word = wordFeature.getWord();
				
				if (wordSpacer.space()) stringBuilder.append(" ");
				
				switch (wordCaseType) {
					case ALL_CHARS_LOWER:
					case PUNCTUATION:
					case PREFIX_SYMBOL:
					case MIXED_CASE:	
						stringBuilder.append(word);
						break;
						
					case ALL_CHARS_UPPER:
						stringBuilder.append(word.toUpperCase());
						break;
						
					case FIRST_CHAR_UPPER:
						stringBuilder.append(Character.toUpperCase(word.charAt(0))).append(word.substring(1, word.length()));
						break;
	
					default:
						break;
				}
				
			} catch (Exception e) {
				log.error("Error classifying", e);
			}
			
		}
		
		return stringBuilder.toString().trim();
	}
	
}
