package org.mlutils.truecase;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.RandomForest;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

/**
 * Train a {@link RandomForest} classifier to recognize the casing of words in a 
 * valid sentence.
 * 
 * @author francois
 *
 */
@Component
public class TrueCasingTrainer {
	
	private static final Logger log = LoggerFactory.getLogger(TrueCasingTrainer.class);
	
	private static final ArrayList<Attribute> featureVector = new ArrayList<Attribute>();
	
	static {
		featureVector.add(new Attribute("wordPosition"));
		featureVector.add(new Attribute("wordLength"));
		featureVector.add(new Attribute("posTagBefore", PartOfSpeech.list()));
		featureVector.add(new Attribute("posTag", PartOfSpeech.list()));
		featureVector.add(new Attribute("posTagAfter", PartOfSpeech.list()));
		featureVector.add(new Attribute("caseType", WordCaseType.list()));
	}
	
	public Classifier train(Instances trainingSet) {
		
		// Train a random forest classifier on training data
		RandomForest rfClassifier = new RandomForest();
		rfClassifier.setNumTrees(400);
		
		try {
			
			rfClassifier.buildClassifier(trainingSet);
			
			log.info("RF Model {}", rfClassifier.toString());
			
		} catch (Exception e) {
			log.error("Error training classifier", e);
		}
		
		return rfClassifier;
	}

	public void evaluateClassifier(Instances trainingSet, Classifier rfClassifier) {
		
		// Test the classifier
		try {
			
			Evaluation evaluation = new Evaluation(trainingSet);
			evaluation.evaluateModel(rfClassifier, trainingSet);
			
			// Print results
			log.info("Evaluation Summary {}", evaluation.toSummaryString());
			
			// Print confusion matrix
			log.info(evaluation.toMatrixString());
			
		} catch (Exception e) {
			log.error("Error testing classifier", e);;
		}
	}

	public Instances toWekaInstances(List<WordFeatures> features, boolean withClass) {
		
		Instances trainingSet = new Instances("Train", featureVector, 10);
		trainingSet.setClassIndex(5);
		
		for (int i = 0; i < features.size(); i++) {
			Instance example = new DenseInstance(6);
			WordFeatures wordFeatures = features.get(i);
			
			example.setValue(featureVector.get(0), wordFeatures.getPositionInSentence());
			example.setValue(featureVector.get(1), wordFeatures.getWordLength());
			example.setValue(featureVector.get(2), getPosTagBefore(features, i));
			
			try {
				example.setValue(featureVector.get(3), wordFeatures.getTag());
			} catch (Exception e) {
				log.error("Undefined Tag " + wordFeatures.getWord() + " - " + wordFeatures.getTag(), e);
			}
			
			example.setValue(featureVector.get(4), getPosTagAfter(features, i));
			
			if (withClass) {
				example.setValue(featureVector.get(5), wordFeatures.getCase().toString());
			}
			
			trainingSet.add(example);
		}
		
		return trainingSet;
	}

	protected String getPosTagBefore(List<WordFeatures> features, int i) {
		
		if (i > 0) {
			WordFeatures wordFeaturesBefore = features.get(i - 1);
			return wordFeaturesBefore.getTag();
		} else {
			return PartOfSpeech.NONE.name();
		}
	}
	
	protected String getPosTagAfter(List<WordFeatures> features, int i) {
		
		if (i < (features.size() - 1)) {
			WordFeatures wordFeaturesBefore = features.get(i + 1);
			return wordFeaturesBefore.getTag();
		} else {
			return PartOfSpeech.NONE.name();
		}
	}

}
