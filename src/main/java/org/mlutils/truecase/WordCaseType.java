package org.mlutils.truecase;

import java.util.ArrayList;
import java.util.List;

/**
 * Different types of casing for words
 * 
 * @author francois
 *
 */
public enum WordCaseType {

	ALL_CHARS_LOWER,
	ALL_CHARS_UPPER,
	FIRST_CHAR_UPPER,
	MIXED_CASE,
	PUNCTUATION,
	PREFIX_SYMBOL;
	
	private static final List<String> _list = new ArrayList<String>();
	
	public static List<String> list() {
		
		if (_list.isEmpty()) {
			synchronized (_list) {
				for (WordCaseType v: values()) {
					_list.add(v.toString());
				}
			}
		}
		
		return _list;
	}
	
	public static WordCaseType fromInt(int positionIndex) {
		
		return WordCaseType.valueOf(list().get(positionIndex));
		
	}
	
}
