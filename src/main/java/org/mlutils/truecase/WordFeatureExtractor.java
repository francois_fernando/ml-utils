package org.mlutils.truecase;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

@Component
public class WordFeatureExtractor {
	
	public static final String NEW_LINE_REPLACEMENT = "____________";
	
	public static final String[] NOUNS_WITH_FIRST_LETTER_CAP = {"act", "minister"};
	
	private static final Logger log = LoggerFactory.getLogger(WordFeatureExtractor.class);

	//	private final MaxentTagger trueCasetagger = new MaxentTagger("postagger-models/english-left3words-distsim.tagger");
	private final MaxentTagger lowerCasetagger = new MaxentTagger("postagger-models/english-caseless-left3words-distsim.tagger");
	
	public List<WordFeatures> extract(String paragraph, boolean isInTrueCase) {
		
		// Preserve new-lines by injecting a recognizable token, add a space at the end
		// so that consecutive newlines doesn't become one long set of underscores
		paragraph = paragraph.replace("\n", NEW_LINE_REPLACEMENT + " ");
		
		List<List<HasWord>> sentencesTrueCase = isInTrueCase ? MaxentTagger.tokenizeText(new StringReader(paragraph)) : null;
		List<List<HasWord>> sentencesLowerCase = MaxentTagger.tokenizeText(new StringReader(paragraph.toLowerCase()));
		
		List<WordFeatures> result = new ArrayList<WordFeatures>(sentencesLowerCase.size() * 5);
		
		for (int i = 0; i < sentencesLowerCase.size(); i++) {
			
			List<HasWord> sentenceLowerCase = sentencesLowerCase.get(i);
			
			log.debug(sentenceLowerCase.toString());
			
			List<HasWord> sentenceTrueCase = (sentencesTrueCase != null) ? sentencesTrueCase.get(i) : sentenceLowerCase;
			
			List<TaggedWord> tagSentence = lowerCasetagger.tagSentence(sentenceLowerCase);

			int j = 0;
			for (TaggedWord taggedWord: tagSentence) {
				result.add(new WordFeatures(j, taggedWord, getTrueCaseWordOrPunctuation(taggedWord, sentenceTrueCase.get(j).word())));
				j++;
			}
			
			log.debug("Tagged sentence: {}", Sentence.listToString(tagSentence, false));
		}
		
		return result;
	}
	
	private String getTrueCaseWordOrPunctuation(TaggedWord taggedWord, String wordOrPlaceHolder) {
		
		String result = wordOrPlaceHolder;
		
		switch (PartOfSpeech.get(taggedWord.tag())) {
			case LRB:
				result = "(";
				break;
				
			case RRB:
				result = ")";
				break;
	
			case LSB:
				result = "[";
				break;
				
			case RSB:
				result = "]";
				break;
				
			case LCB:
				result = "{";
				break;
				
			case RCB:
				result = "}";
				break;
				
			case OPEN_QUOTE:
			case CLOSE_QUOTE:
				result = "'".equals(wordOrPlaceHolder) ? "'" : "\"";
				break;
				
			case NOUN:
				if (isNounWithFirstLetterUppercase(wordOrPlaceHolder)) 
					result = Character.toUpperCase(wordOrPlaceHolder.charAt(0))  + "" + wordOrPlaceHolder.substring(1, wordOrPlaceHolder.length());
				break;
				
			default:
				break;
		}
		
		if (NEW_LINE_REPLACEMENT.equals(wordOrPlaceHolder)) {
			result = "\n";
		}
		
		return result;
	}
	
	private boolean isNounWithFirstLetterUppercase(String word) {
		boolean result = false;
		
		for (int i = 0; i < NOUNS_WITH_FIRST_LETTER_CAP.length; i++) {
			if (NOUNS_WITH_FIRST_LETTER_CAP[i].equalsIgnoreCase(word)) {
				result = true;
				break;
			}
		}
		
		return result;
	}
}
