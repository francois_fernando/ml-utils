package org.mlutils.truecase;

import edu.stanford.nlp.ling.TaggedWord;

/**
 * Features of a word in a sentence to be used for training the true-casing classifier 
 * 
 * @author francois
 *
 */
public class WordFeatures {

	private final int positionInSentence;
	private final TaggedWord taggedWord;
	private final String word;
	
	public WordFeatures(int positionInSentence, TaggedWord taggedWord, String word) {
		this.positionInSentence = positionInSentence;
		this.taggedWord = taggedWord;
		this.word = word;
	}

	public int getPositionInSentence() {
		return positionInSentence;
	}
	
	public int getWordLength() {
		return word.length();
	}
	
	public String getWord() {
		return word;
	}
	
	public String getTag() {
		return taggedWord.tag();
	}
	
	public WordCaseType getCase() {
		
		boolean onlyfirstCharUpper = false;
		boolean allCharsUpper = true;
		boolean allCharsLower = true;
		
		if (isPunctuation())
			return WordCaseType.PUNCTUATION;
		
		for (int i = 0 ; i < word.length(); i++) {
			char charAt = word.charAt(i);
			
			if (Character.isLowerCase(charAt)) {
				allCharsUpper = false;
			} else if (Character.isUpperCase(charAt)) {
				allCharsLower = false;
				
				if (i == 0) {
					onlyfirstCharUpper = true;
				}  else {
					onlyfirstCharUpper = false;
				}
			}
		}
		
		if (allCharsLower)
			return WordCaseType.ALL_CHARS_LOWER;
		else 
			if (allCharsUpper)
				return WordCaseType.ALL_CHARS_UPPER;
			else 
				if (onlyfirstCharUpper)
					return WordCaseType.FIRST_CHAR_UPPER;
				else
					return WordCaseType.MIXED_CASE;
		
	}
	
	public boolean isPunctuation() {
		
		boolean result = word.equals("\n");
		
		switch (getPartOfSpeech()) {
			case SENTENCE_TERMINATOR:
			case PUNC_COMMA:
			case PUNC_SCOLON:
			case PUNC_COLON:
			case LRB:
			case RRB:
			case LSB:
			case RSB:
			case LCB:
			case RCB:
				result = true;
				break;
				
			default:
				break;
		}
		
		return result;
	}
	
	public boolean isPrefixSymbol() {
		return getPartOfSpeech().equals(PartOfSpeech.DOLLAR);
	}
	
	public PartOfSpeech getPartOfSpeech() {
		return PartOfSpeech.get(taggedWord.tag());
	}
	
	
	
}
